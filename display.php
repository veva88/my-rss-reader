<?php
	function displayFeed($sorted, $feed){
		print '<h3>' . $feed->title . '</h3><br>';
		foreach($sorted as $item){
			print '<h4>' . $item->title . '</h4>';
			print '<p>' . $item->description . '<br>';
			print $item->pubDate . '</p>';
			if($item->enclosure){
				print '<img src="' . $item->enclosure['url'] . '" alt="' . $item->enclosure['type'] . '" width="550" height="400"/> <br>';
			}
			print '<p> Celý článok: <a href="' . $item->link . '" target="_blank">Klikni sem!</a></p>';
		}
	}
?>