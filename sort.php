<?php
	function sortNew($a,$b) {
        $a_ = strtotime($a->pubDate); 
        $b_ = strtotime($b->pubDate);
        return $b_ <=> $a_;
	}
	
	function sortLate($a,$b) { 
        $a_ = strtotime($a->pubDate);
        $b_ = strtotime($b->pubDate);
        return $a_ <=> $b_;
	}
		
	function sort_newest($feed){
		$sorted_newest = array();
		foreach($feed->item as $node){
			$sorted_newest[] = $node;
		}
		usort($sorted_newest, "sortNew");
		return $sorted_newest;
	}
	
	function sort_latest($feed){
		$sorted_latest = array();		
		foreach($feed->item as $node){
			$sorted_latest[] = $node;
		}
		usort($sorted_latest, "sortLate");
		return $sorted_latest;
	}
?>