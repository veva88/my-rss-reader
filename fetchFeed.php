<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>RSS citacka</title>
  
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
  <link href="css/landing-page.min.css" rel="stylesheet">
</head>

<body>
 <header class="masthead text-white text-center">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-xl-10 mx-auto">
          <h1 class="mb-5"><a href="index.html" target = "_self"><< Späť (RSS Atom Čítačka)</a></h1>
		  <p class="mb-5">Vytvorené: Veronika Töröková</p>
		  <form method="POST">
		  <div class="form-row">
              <div class="col-12 col-md-6">
                <input type="submit" class="btn btn-block btn-lg btn-primary" name="order_newest" value="Od najnovších" />
              </div>
              <div class="col-12 col-md-6">
                <input type="submit" class="btn btn-block btn-lg btn-primary" name="order_latest" value="Od najstarších" />
              </div>
            </div>
			</form>
        </div>
	  </div>
	</div>
  </header>
  
  <section class="masthead text-black text-center">
    <div class="container">
      <div class="row">
        <div class="col-xl-10 mx-auto">
		  <?php
			include 'sendPost.php';
			include 'sort.php';
			include 'display.php';
			
			if( isset($_GET['FetchFeed']) ){
				$fetchUrl = $_GET["url"];
				$result = post_req('http://webp.itprof.sk:8000/fetchurl',$fetchUrl);
				$feed = $result->channel;
				$sortable = array();
				foreach($feed->item as $node){
					$sortable[] = $node;
				}
				ob_start();
				displayFeed($sortable, $feed);
		    }
			if(isset($_POST['order_newest'])){
				ob_end_clean();
				$sortedNew = sort_newest($feed);
				displayFeed($sortedNew, $feed);
			}
			if(isset($_POST['order_latest'])){
				ob_end_clean();
				$sortedLate = sort_latest($feed);
				displayFeed($sortedLate, $feed);
			}
		  ?>
        </div>
      </div>
	</div>
  </section>  
 </body>