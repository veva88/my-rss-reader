<?php
	function post_req($url, $fetchFile) {
		$postRequest = json_encode(array('url' => $fetchFile));
		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postRequest);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close($ch);

		$xmldata = simplexml_load_string($result);
		$xml = explode("\n", $result);

		return $xmldata;
	}
?>